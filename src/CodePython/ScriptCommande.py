#!/usr/bin/env python
# coding: utf-8
import os
import sys
from serial import Serial

serialDevDir = '/dev/serial/by-id'
if (os.path.isdir(serialDevDir)):
    #On récupére tout les chemins des éléments connectés
    serialDevices = os.listdir(serialDevDir)
    if (len(serialDevices) > 0):
        #On récupère le chemin de la carte microbit
        serialDevicePath = os.path.join(serialDevDir, serialDevices[0])
        #On ouvre la connection avec la carte Microbit
        serial = Serial(port = serialDevicePath, baudrate = 19200, timeout = 0.2)
        #On récupére les paramètre passé au script, le premier correspond à la carte microBit ciblé le deuxième à la commande désiré
        CarteCible = sys.argv[1]
        Commande = sys.argv[2]
        #On envoi le message à la carte microbit connecté au raspberry ( qui redirigera le message au bon destinataire)
        sendMsg = CarteCible.encode('ascii') + ":" + Commande.encode('ascii');
        serial.write(sendMsg + b'\n')
        print(sendMsg)
    else:
        print('No serial devices connected')
else:
    print(serialDevDir + ' does not exist')