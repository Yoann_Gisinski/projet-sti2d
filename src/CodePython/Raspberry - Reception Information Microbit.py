#!/usr/bin/env python
# coding: utf-8
import os, time, json
from datetime import datetime
from serial import Serial

nextCompassPoll = 0.0 ;
serialDevDir = '/dev/serial/by-id'
if (os.path.isdir(serialDevDir)):
    serialDevices = os.listdir(serialDevDir)
    if (len(serialDevices) > 0):
        serialDevicePath = os.path.join(serialDevDir, serialDevices[0])
        serial = Serial(port = serialDevicePath, baudrate = 19200, timeout = 0.2)        
        while (True):
            receivedMsg = serial.readline()
            if ((len(receivedMsg) >= 4) and (receivedMsg[3] == b':'[0])):
                msgType = receivedMsg[0:3]
                msgFrom = receivedMsg[4:7]
                msgData = receivedMsg[7:]               
                #gestion des messages en provenances de la carte microbit à modfifier
                if (msgType == b'JSN'):
                    lines = msgData.decode('ascii').splitlines()
                    with open('monitoring' + msgFrom.decode('ascii') + '.jsn', 'w', newline = '') as f:
                        print(receivedMsg)
                        f.write(msgData.decode('ascii'))
    else:
        print('No serial devices connected')
else:
    print(serialDevDir + ' does not exist')