door_state = False
water_filling = False; water_level = 100
hen_presence = 0; hen = 0
egg_collecting = False
cleaning = False
radio.set_group(42)
led.enable(False)
pins.digital_read_pin(DigitalPin.P6)
pins.digital_read_pin(DigitalPin.P7)
pins.digital_read_pin(DigitalPin.P9)
pins.digital_read_pin(DigitalPin.P10)

def on_received_value(name, value):
    global door_state, water_level, water_filling, hen, hen_presence, egg_collecting, cleaning
    if value == 1: # porte
        if not door_state: # fermeture
            pins.digital_write_pin(DigitalPin.P6, 1)
            basic.pause(5000) # tempo (à régler)
            pins.digital_write_pin(DigitalPin.P6, 0)
            pins.digital_read_pin(DigitalPin.P6)
            door_state = not door_state
        else: # ouverture
            pins.digital_write_pin(DigitalPin.P5, 1)
            basic.pause(5000) # tempo (à régler)
            pins.digital_write_pin(DigitalPin.P5, 0)
            door_state = not door_state
    elif value == 2: # ne fonctionne pas # remplissage d'eau
        if water_level <= 50: # remplir
            pins.digital_write_pin(DigitalPin.P7, 1) # relais electrovanne
            water_level = 75 # temp
        else: # plein
            pins.digital_write_pin(DigitalPin.P7, 0) # relais electrovanne
            pins.digital_read_pin(DigitalPin.P7)
    elif value == 3: # ramassage des oeufs
        if not egg_collecting:
            pins.digital_write_pin(DigitalPin.P8, 1)
            basic.pause(15000) # tempo (à régler)
            pins.digital_write_pin(DigitalPin.P8, 0)
            egg_collecting = True
        else:
            pins.digital_write_pin(DigitalPin.P9, 1)
            basic.pause(15000) # tempo (à régler)
            pins.digital_write_pin(DigitalPin.P9, 0)
            pins.digital_read_pin(DigitalPin.P9)
            egg_collecting = False
    elif value == 4: # nettoyage du poulailler
        if not cleaning: # nettoyer
            cleaning = True
            pins.digital_write_pin(DigitalPin.P10, 1) # relais moteur tapis
            basic.pause(10000) # tempo de nettoyage (à régler)
            pins.digital_write_pin(DigitalPin.P10, 0) # relais moteur tapis
            pins.digital_read_pin(DigitalPin.P10)
            cleaning = False
    water_level = int(pins.analog_read_pin(AnalogPin.P1)) # capteur de niveau d'eau
    hen = int(pins.analog_read_pin(AnalogPin.P2)) # capteur de présence
    if hen == 1:
        hen_presence = 1
    else:
        hen_presence = 0
    StringToSplit = 'JSN:MC1{' 
                               + '"door_state":"' + door_state + '",'
                               + '"water_level":"' + water_level + '",'
                               + '"water_filling":"' + water_filling + '",'
                               + '"hen_presence":"' + hen_presence + '",'
                               + '"egg_collecting":"' + egg_collecting + '",'
                               + '"cleaning":"' + cleaning + '"}'
    for i in range((str(StringToSplit).length//15)+1):
        if i == 0:
            StringToSend = "1:S" + StringToSplit[0:15]
        elif i != (str(StringToSplit).length//15):
            StringToSend = "1:0" + StringToSplit[i*15:i*15+15]
        else:
            StringToSend = "1:E" + StringToSplit[i*15:str(StringToSplit).length]
        radio.send_string(StringToSend)
        basic.pause(20)
    basic.pause(1000)
radio.on_received_value(on_received_value)