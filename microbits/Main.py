msgData2 = ""
msgType = ""
serialInput = ""
SendQueue: List[str] = []
InputState = ""
ReceivedStringTemp: List[str] = []
SenderNum = ""
msgData = ""
serial.redirect(SerialPin.USB_TX, SerialPin.USB_RX, BaudRate.BAUD_RATE19200)
radio.set_group(42)

def on_received_string(receivedString):
    global SenderNum, InputState, msgData
    SenderNum = receivedString.substr(0, 1)
    InputState = receivedString.substr(2, 1)
    msgData = receivedString.slice(3, len(receivedString))
    if InputState == "0" or InputState == "S" or InputState == "E":
        if InputState == "S":
            ReceivedStringTemp[int(SenderNum)] = ""
        ReceivedStringTemp[int(SenderNum)] += msgData
        if InputState == "E":
            SendQueue.append(ReceivedStringTemp[int(SenderNum)])
            ReceivedStringTemp[int(SenderNum)] = ""
    else:
        SendQueue.append(receivedString)
radio.on_received_string(on_received_string)

def on_forever():
    DisplayQueue: List[number] = []
    while len(DisplayQueue) > 0:
        basic.show_string("" + str(DisplayQueue.shift()))
basic.forever(on_forever)

def on_forever2():
    global serialInput, msgType, msgData2
    serialInput = serial.read_until(serial.delimiters(Delimiters.NEW_LINE))
    if len(serialInput) >= 4 and serialInput.char_at(3) == ":":
        msgType = serialInput.substr(0, 3)
        msgData2 = serialInput.substr(4, len(serialInput) - 4)
        radio.send_value(msgType, int(msgData2))
basic.forever(on_forever2)

def on_forever3():
    while len(SendQueue) > 0:
        serial.write_line("" + (SendQueue.shift()))
basic.forever(on_forever3)