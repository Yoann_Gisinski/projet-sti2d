let door_state = true;
let cleaning_running = false;
let water_filling = false;
let egg_collecting = false;

let water_level_temp = 50;
let hen_presence_temp_bool = true;
if (hen_presence_temp_bool) {
	hen_presence_temp = "Oui";
} else {
	hen_presence_temp = "Non";
}

function initialization() {
	// window.setInterval('AjaxUpdateContent("MC1","#MC1Info")', 1000);
	const tab = [hen, water_level, door, cleaning, water_fill, collecting]
	for (let i = 0; i < tab.length; i++) {
		tab[i].style.transition = "all .5s";
		tab[i].style.fill = "rgba(255, 255, 255, 1)";
	}
	AjaxUpdateContent("test");
}

function Door() {
	if (door_state) {
		door.style.fill = "rgba(0, 255, 0, .5)";
	} else {
		door.style.fill = "rgba(255, 0, 0, .5)";
	}
}
function OnClickDoor() {
	door_state = !door_state;
	Door();
	AjaxAskMicrobit("MC1", 1);
}

function Cleaning() {
	if (cleaning_running) {
		cleaning.style.fill = "rgba(0, 255, 0, .5)";
	} else {
		cleaning.style.fill = "rgba(255, 0, 0, .5)";
	}
}
function OnClickCleaning() {
	cleaning_running = !cleaning_running;
	Cleaning();
	AjaxAskMicrobit("MC1", 4);
}

function WaterFilling() {
	if (water_filling) {
		water_fill.style.fill = "rgba(0, 255, 0, .5)";
	} else {
		water_fill.style.fill = "rgba(255, 0, 0, .5)";
	}
}
function OnClickWaterFilling() {
	water_filling = !water_filling;
	WaterFilling();
	AjaxAskMicrobit("MC1", 2);
}

function CollectingEggs() {
	if (egg_collecting) {
		collecting.style.fill = "rgba(0, 255, 0, .5)";
	} else {
		collecting.style.fill = "rgba(255, 0, 0, .5)";
	}
}
function OnclickCollectingEggs() {
	egg_collecting = !egg_collecting;
	CollectingEggs();
	AjaxAskMicrobit("MC1", 3);
}

function AjaxAskMicrobit(Carte_P, Ordre_P) {
	$.ajax({
		type: "POST",
		url: 'src/AjaxCommandePythonController.php',
		data: ({CarteCommande:Carte_P,Ordre:Ordre_P}),
		success: function(response){
			console.log(response);
		},
		error: function(response){
			$('#status').text('Erreur pour poster le formulaire : ' + response.status + " " + response.statusText);
			console.log(response);
		}						
	});
}

function AjaxUpdateContent(Carte_P) {
	$.ajax ( {
		type: "GET",
		url: 'src/AjaxReadMonitoringFileController.php',
		data: ({CarteCommande:Carte_P}),
		success: function(response) {
			try {
				jsonData = JSON.parse(response);
				if (Carte_P == "MC1") {
					if (Object.keys(jsonData).length != 2) {
						throw "JSON error";
					}
					data.push(jsonData.door_state);
					data.push(jsonData.water_level);
					data.push(jsonData.hen_presence);
					data.push(jsonData.cleaning);
					data.push(jsonData.egg_collecting);
					if(data.length == 25) {
						data.splice(0,1);
					}
					myLineChart.update();
				}
				if (Carte_P == "MC2") {
					if (Object.keys(jsonData).length != 3) {
						throw "JSON error";
					}
				}
				door_state = jsonData.door_state;
				water_level = jsonData.water_level;
				hen_presence = jsonData.hen_presence;
				cleaning_running = jsonData.cleaning_running;
				egg_collecting = jsonData.egg_collecting;
				Door();
				Cleaning();
				WaterFilling();
				CollectingEggs();
			}
			catch (error) {
				
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}